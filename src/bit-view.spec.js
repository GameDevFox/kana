import BitView from './bit-view';
import { toInt } from './util';

describe('BitView', () => {
  it('should work', () => {
    const buffer = Buffer.from('Hello World');
    const bitView = BitView(buffer);

    toInt(bitView(0, 1)).should.equal(0);
    toInt(bitView(1, 1)).should.equal(1);
    toInt(bitView(2, 1)).should.equal(0);
    toInt(bitView(3, 1)).should.equal(0);
    toInt(bitView(4, 1)).should.equal(1);

    bitView(4, 8).toString('hex').should.equal('86');
    bitView(12, 16).toString('hex').should.equal('56c6');
    bitView(19, 19).toString('hex').should.equal('031b1b');
  });
});
