import { leftBitMask, rightBitMask, rightShiftBuffer, leftTrimBuffer } from './util';

const BitView = buffer => (start, length) => {
  const startByte = Math.floor(start / 8);

  const endBit = start + length - 1;
  const endByte = Math.floor(endBit / 8);

  const slice = Buffer.from(buffer.slice(startByte, endByte + 1));

  if(slice.length === 0)
    return 0;

  // Mask first byte (head)
  const startOffset = start % 8;
  slice[0] &= rightBitMask(8 - startOffset);

  // Shift all bytes
  const endOffset = endBit % 8;
  const lastIdx = slice.length - 1;
  slice[lastIdx] &= leftBitMask(endOffset + 1);

  const shiftSize = 8 - (endOffset + 1);
  const result = leftTrimBuffer(rightShiftBuffer(slice, shiftSize));

  return result;
};

export default BitView;
