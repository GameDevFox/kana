import BitReader from './bit-reader';
import { toBinary } from './util';

describe('BitReader', () => {
  it('should work', () => {
    // eslint-disable-next-line no-useless-concat
    const buffer = Buffer.from('55aa' + 'aa55' + '5aa5' + 'a55a', 'hex');
    const reader = BitReader(buffer);

    // toBinary(buffer).should.equal('01010101101010101010101001010101');
    // 0 1 01 010 1101 01010 101010 1001010 101...
    toBinary(reader(1)).should.equal('');
    toBinary(reader(1)).should.equal('00000001');
    toBinary(reader(2)).should.equal('00000001');
    toBinary(reader(3)).should.equal('00000010');
    toBinary(reader(4)).should.equal('00001101');
    toBinary(reader(5)).should.equal('00001010');
    toBinary(reader(6)).should.equal('00101010');
    toBinary(reader(7)).should.equal('01001010');
    toBinary(reader(8)).should.equal('10101011');
    toBinary(reader(9)).should.equal('10101001');
    // eslint-disable-next-line no-useless-concat
    toBinary(reader(10)).should.equal('00000001' + '10100101');
    reader().should.equal(8);
    toBinary(reader(8)).should.equal('01011010');
  });

  it('should prevent overflow', () => {
    const buffer = Buffer.from('ff', 'hex');
    const reader = BitReader(buffer);

    toBinary(reader(1)).should.equal('00000001');
    toBinary(reader(2)).should.equal('00000011');
    toBinary(reader(3)).should.equal('00000111');
    (() => reader(4)).should.throw();
    toBinary(reader(2)).should.equal('00000011');
  });
});
