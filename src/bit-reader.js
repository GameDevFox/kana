import { leftBitMask, rightBitMask, rightShiftBuffer, leftTrimBuffer } from './util';

const BitReader = buffer => {
  let offset = 0;

  return count => {
    if(count === undefined)
      return (buffer.length * 8) - offset;

    const nextOffset = offset + count;

    const startByte = Math.floor(offset / 8);
    const endByte = Math.floor((nextOffset - 1) / 8);

    if(endByte > buffer.length - 1)
      throw new Error('Buffer overflow');

    const slice = Buffer.from(buffer.slice(startByte, endByte + 1));

    if(slice.length === 0)
      return 0;

    // Mask first byte (head)
    const startOffset = offset % 8;
    slice[0] &= rightBitMask(8 - startOffset);

    // Shift all bytes
    const endOffset = (nextOffset - 1) % 8;
    const lastIdx = slice.length - 1;
    slice[lastIdx] &= leftBitMask(endOffset + 1);

    const shiftSize = 8 - (endOffset + 1);
    const result = leftTrimBuffer(rightShiftBuffer(slice, shiftSize));

    offset = nextOffset;
    return result;
  };
};

export default BitReader;
