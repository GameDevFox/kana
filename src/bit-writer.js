import { leftShiftBuffer, rightShiftBuffer } from './util';

const BitWriter = () => {
  const array = [];
  let offset = 0;

  const writer = (buffer, count) => {
    if(count === undefined)
      count = buffer.length * 8;

    const bufferSize = buffer.length * 8;
    if(count > bufferSize)
      throw new Error(`Bit \`count\` is larger than \`buffer\` length: ${count} > ${bufferSize}`);

    // Shift output if nessisary
    const leftShift = bufferSize - count;
    const rightShift = offset % 8;

    const leftShiftedBuffer = leftShiftBuffer(buffer, leftShift);
    const rightShiftedBuffer = rightShiftBuffer(leftShiftedBuffer, rightShift);
    const trimmedBuffer = rightShiftedBuffer.slice(0, Math.ceil((count + rightShift) / 8));

    const inputBytes = [...trimmedBuffer];

    for(let i = 0; i < inputBytes.length; i++) {
      const val = inputBytes[i];

      if(i === 0 && offset % 8 !== 0)
        array[array.length - 1] |= val;
      else
        array.push(val);
    }

    offset += count;

    return offset;
  };

  writer.align = size => {
    const blankBuffer = Buffer.alloc(Math.ceil(size / 8));
    writer(blankBuffer, (size - (offset % size)) % size);
  };

  writer.offset = () => offset;

  writer.toBuffer = () => {
    let buffer = Buffer.from(array);

    const intendedSize = Math.ceil(offset / 8);
    if(buffer.length < intendedSize) {
      const newBuffer = Buffer.alloc(intendedSize);
      buffer.copy(newBuffer);
      buffer = newBuffer;
    }

    return buffer;
  };

  return writer;
};

export default BitWriter;
