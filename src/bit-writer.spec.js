import BitWriter from './bit-writer';
import { toBinary } from './util';

describe('BitWriter', () => {
  it('should work', () => {
    const writer = BitWriter();

    toBinary(writer.toBuffer()).should.equal('');
    writer.offset().should.equal(0);

    /* eslint-disable no-useless-concat */
    writer(Buffer.from('ffffffff', 'hex'), 3);
    toBinary(writer.toBuffer()).should.equal('11100000');
    writer.offset().should.equal(3);

    writer(Buffer.from('aaaaaaaa', 'hex'), 3);
    toBinary(writer.toBuffer()).should.equal('11101000');
    writer.offset().should.equal(6);

    writer(Buffer.from('ffffffff', 'hex'), 5);
    toBinary(writer.toBuffer()).should.equal('11101011' + '11100000');
    writer.offset().should.equal(11);

    writer(Buffer.from('aaaaaaaa', 'hex'), 9);
    toBinary(writer.toBuffer()).should.equal(
      '11101011' + '11101010' + '10100000'
    );
    writer.offset().should.equal(20);

    writer(Buffer.from('ffffffff', 'hex'), 13);
    toBinary(writer.toBuffer()).should.equal(
      '11101011' + '11101010' + '10101111' + '11111111' + '10000000'
    );
    writer.offset().should.equal(33);

    writer(Buffer.from('aaaaaaaa', 'hex'), 7);
    toBinary(writer.toBuffer()).should.equal(
      '11101011' + '11101010' + '10101111' + '11111111' + '10101010'
    );
    writer.offset().should.equal(40);
    /* eslint-enable no-useless-concat */
  });

  it('should be able to align', () => {
    let writer = BitWriter();
    writer(Buffer.from('ffffffff', 'hex'), 13);
    writer.align(1);
    writer.offset().should.equal(13);

    writer = BitWriter();
    writer(Buffer.from('ffffffff', 'hex'), 13);
    writer.align(2);
    writer.offset().should.equal(14);

    writer = BitWriter();
    writer(Buffer.from('ffffffff', 'hex'), 13);
    writer.align(4);
    writer.offset().should.equal(16);

    writer = BitWriter();
    writer(Buffer.from('ffffffff', 'hex'), 13);
    writer.align(9);
    writer.offset().should.equal(18);

    writer = BitWriter();
    writer(Buffer.from('ffffffff', 'hex'), 20);
    writer.align(64);
    writer.offset().should.equal(64);

    writer = BitWriter();
    writer(Buffer.from('ffffffffffffffffffff', 'hex'), 75);
    writer.align(64);
    writer.offset().should.equal(128);
    toBinary(writer.toBuffer()).should.equal(
      '1111111111111111111111111111111111111111111111111111111111111111' +
      '1111111111100000000000000000000000000000000000000000000000000000'
    );
  });
});
