import { KanaWriter, KanaReader } from './kana';
import { toHex } from './util';

describe('KanaWriter', () => {
  it('should work', () => {
    const writer = KanaWriter();
    writer(1);
    writer('A');
    writer('one');
    writer(2);
    writer('another');
    writer(127);
    // writer('big one');
    // writer([3, 4, 5]);
    // writer('Hello World');

    writer().toString('hex').should.equal(
      '01' + // Value 1
      'c0' + toHex('A') + // Value Size 1 + String 'A'
      'c2' + toHex('one') + // Value Size 3 + String 'one'
      '02' + // Value 2
      'c6' + toHex('another') + // Value Size 3 + String 'one'
      '7f'
    );
  });
});

describe('KanaReader', () => {
  it('should work', () => {
    let reader = KanaReader(Buffer.from('00', 'hex'));
    reader().should.equal(0);

    reader = KanaReader(Buffer.from('c0', 'hex'));
    reader().should.equal(2);

    reader = KanaReader(Buffer.from('fe', 'hex'));
    reader().should.equal(7);
  });
});
