import {
  leftBitMask, rightBitMask, leftShiftBuffer, rightShiftBuffer,
  leftTrimBuffer, toBinary,
} from './util';

describe('util', () => {
  it('leftBitMask and rightBitMask', () => {
    leftBitMask(0).should.equal(0);
    leftBitMask(1).should.equal(128);
    leftBitMask(2).should.equal(192);
    leftBitMask(3).should.equal(224);
    leftBitMask(4).should.equal(240);
    leftBitMask(5).should.equal(248);
    leftBitMask(6).should.equal(252);
    leftBitMask(7).should.equal(254);
    leftBitMask(8).should.equal(255);

    rightBitMask(0).should.equal(0);
    rightBitMask(1).should.equal(1);
    rightBitMask(2).should.equal(3);
    rightBitMask(3).should.equal(7);
    rightBitMask(4).should.equal(15);
    rightBitMask(5).should.equal(31);
    rightBitMask(6).should.equal(63);
    rightBitMask(7).should.equal(127);
    rightBitMask(8).should.equal(255);
  });

  it('leftShiftBuffer', () => {
    let buffer = Buffer.from('aa55', 'hex');
    toBinary(buffer).should.equal('1010101001010101');

    buffer = leftShiftBuffer(buffer, 3);
    toBinary(buffer).should.equal('0101001010101000');

    buffer = leftShiftBuffer(buffer, 5);
    toBinary(buffer).should.equal('0101010100000000');

    buffer = leftShiftBuffer(buffer, 8);
    toBinary(buffer).should.equal('0000000000000000');
  });

  it('rightShiftBuffer', () => {
    let buffer = Buffer.from('aa00', 'hex');
    toBinary(buffer).should.equal('1010101000000000');

    buffer = rightShiftBuffer(buffer, 3);
    toBinary(buffer).should.equal('0001010101000000');

    buffer = rightShiftBuffer(buffer, 5);
    toBinary(buffer).should.equal('0000000010101010');

    buffer = rightShiftBuffer(buffer, 5);
    toBinary(buffer).should.equal('0000000000000101');

    buffer = rightShiftBuffer(buffer, 3);
    toBinary(buffer).should.equal('0000000000000000');
  });

  it('leftTrimBuffer', () => {
    leftTrimBuffer(Buffer.from('1122334400', 'hex')).length.should.equal(5);
    leftTrimBuffer(Buffer.from('0022000000', 'hex')).length.should.equal(4);
    leftTrimBuffer(Buffer.from('0000330000', 'hex')).length.should.equal(3);
    leftTrimBuffer(Buffer.from('0000004400', 'hex')).length.should.equal(2);
    leftTrimBuffer(Buffer.from('0000000000', 'hex')).length.should.equal(0);
  });
});
