/* eslint-disable */
import BitReader from './bit-reader';
import BitWriter from './bit-writer';
import { toBinary, toHex } from './util';

const integerBitLength = integer => Math.floor(Math.log2(integer)) + 1;

export const KanaWriter = (config = { frameSize: 8 }) => {
  const { frameSize } = config;

  const bitWriter = BitWriter();

  const writer = input => {
    if(input === undefined)
      return bitWriter.toBuffer();

    // console.log('Input:', input);
    if(typeof input === 'number' && (input | 0) === input) {
      console.log('Integer Type:', input);

      const bitLength = integerBitLength(input);
      console.log('  Bit Length:', bitLength);

      if(bitLength < frameSize) {
        // Write directly
        bitWriter(Buffer.from('00'), 1);
        bitWriter(Buffer.from([input]), frameSize - 1);
      } else if(bitLength % frameSize === 0) {
        // Specify value size
        throw new Error('Unsupported');
      } else {
        // Expand frame
        throw new Error('Unsupported');
      }
    } else if(typeof input === 'string') {
      console.log('String Type:', input, toHex(Buffer.from(input)));
      if(input.length <= Math.pow(2, frameSize - 3) + 1) {
        bitWriter(Buffer.from('06'), 3);
        bitWriter(Buffer.from([input.length - 1]), 5);
        bitWriter(Buffer.from(input), input.length * 8);
      } else
        throw new Error('Unsupported');
    } else if(Array.isArray(input))
      console.log('Array Type:', input);
    else
      console.log('== Unknown:', input);

    console.log('Offset:', bitWriter.offset());
    console.log();
  };

  return writer;
};

export const KanaReader = buffer => {
  const bitReader = BitReader(buffer);

  const countBitsUntilZero = () => {
    let count = -1;

    let bit;
    do {
      count++;
      bit = [...bitReader(1)][0];
      console.log(bit);
    } while(bit);

    return count;
  };

  const reader = () => {
    const count = countBitsUntilZero();
    console.log('Count:', count);
    return count;
  };

  return reader;
};
