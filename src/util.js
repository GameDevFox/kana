export const leftBitMask = size => Math.pow(2, size) - 1 << (8 - size);
export const rightBitMask = size => Math.pow(2, size) - 1;

export const toBinary = buffer => {
  const result = [];
  buffer.forEach(x => result.push(x.toString(2).padStart(8, '0')));
  return result.join('');
};

export const toHex = string => Buffer.from(string).toString('hex');

export const toInt = buffer => {
  let result = 0;

  for(let i = 0; i < buffer.length; i++)
    result += buffer[i] * Math.pow(256, i);

  return result;
};

export const leftShiftBuffer = (buffer, count) => {
  if(count === 0)
    return buffer;

  // Major shift
  const majorShift = Math.floor(count / 8);
  const result = Buffer.alloc(buffer.length + majorShift);
  buffer.copy(result);

  // Minor shift
  const minorShift = count % 8;
  let arr = [...result];
  arr = arr.map(val => val << minorShift);

  const base = arr.map(val => val & 255);
  const shift = arr.map(val => val >> 8).concat(0);

  for(let i = 0; i < base.length; i++) {
    const index = base.length - i - 1;
    shift[index + 1] += base[index];
  }

  const total = Buffer.from(shift);
  return total.slice(total.length - buffer.length, total.length);
};

export const rightShiftBuffer = (buffer, count) => {
  if(count === 0)
    return buffer;

  // Major shift
  const majorShift = Math.floor(count / 8);
  let result = Buffer.from(buffer.slice(0, buffer.length - majorShift));

  // Minor shift
  const minorShift = count % 8;
  if(minorShift === 0)
    return result;

  const mask = rightBitMask(minorShift);

  // Copy right side
  let copy = [];
  result.forEach(x => copy.push(x & mask));

  // Shift original
  result = result.map(x => x >> minorShift);

  // Shift right side to left side
  copy = copy.map(x => x << (8 - minorShift));

  // Add to original adjusted by 1
  for(let i = 0; i < copy.length - 1; i++)
    result[i + 1] = result[i + 1] | copy[i];

  return result;
};

export const leftTrimBuffer = buffer => {
  let nonZeroIndex = 0;
  for(; nonZeroIndex < buffer.length; nonZeroIndex++) {
    if(buffer[nonZeroIndex] !== 0)
      break;
  }

  return buffer.slice(nonZeroIndex, buffer.length);
};
